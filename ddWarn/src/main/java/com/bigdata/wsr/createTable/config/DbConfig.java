package com.bigdata.wsr.createTable.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.sql.SQLException;
import java.util.Collections;

/**
 * 测试配置
 *
 * @author rui.wang
 * @date 2021/11/17
 */
public class DbConfig {
    public static DruidDataSource getMysql137AccountDs() {
        Config config = ConfigFactory.load("mysql_137_account.properties");
        String driver = config.getString("mysql.driver");
        String url = config.getString("mysql.url");
        String user = config.getString("mysql.user");
        String password = config.getString("mysql.password");

        DruidDataSource ds = new DruidDataSource();
        ds.setDriverClassName(driver);
        ds.setUrl(url);
        ds.setUsername(user);
        ds.setPassword(password);
        ds.setMaxActive(550);
        ds.setInitialSize(1);
        ds.setMinIdle(1);
        ds.setMaxActive(3);
        ds.setMaxWait(60000);
        ds.setTimeBetweenEvictionRunsMillis(60000);
        ds.setMinEvictableIdleTimeMillis(300000);
        ds.setValidationQuery("select 1");
        ds.setConnectionInitSqls(Collections.singletonList("set names utf8mb4"));
        try {
            ds.init();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return ds;
    }

    public static DruidDataSource getMysql137BiDs() {
        Config config = ConfigFactory.load("mysql_137_bi.properties");
        String driver = config.getString("mysql.driver");
        String url = config.getString("mysql.url");
        String user = config.getString("mysql.user");
        String password = config.getString("mysql.password");

        DruidDataSource ds = new DruidDataSource();
        ds.setDriverClassName(driver);
        ds.setUrl(url);
        ds.setUsername(user);
        ds.setPassword(password);
        ds.setMaxActive(550);
        ds.setInitialSize(1);
        ds.setMinIdle(1);
        ds.setMaxActive(3);
        ds.setMaxWait(60000);
        ds.setTimeBetweenEvictionRunsMillis(60000);
        ds.setMinEvictableIdleTimeMillis(300000);
        ds.setValidationQuery("select 1");
        ds.setConnectionInitSqls(Collections.singletonList("set names utf8mb4"));
        try {
            ds.init();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return ds;
    }

    public static DruidDataSource getMysql137BigFrogDs() {
        Config config = ConfigFactory.load("mysql_137_bigfrog.properties");
        String driver = config.getString("mysql.driver");
        String url = config.getString("mysql.url");
        String user = config.getString("mysql.user");
        String password = config.getString("mysql.password");

        DruidDataSource ds = new DruidDataSource();
        ds.setDriverClassName(driver);
        ds.setUrl(url);
        ds.setUsername(user);
        ds.setPassword(password);
        ds.setMaxActive(550);
        ds.setInitialSize(1);
        ds.setMinIdle(1);
        ds.setMaxActive(3);
        ds.setMaxWait(60000);
        ds.setTimeBetweenEvictionRunsMillis(60000);
        ds.setMinEvictableIdleTimeMillis(300000);
        ds.setValidationQuery("select 1");
        ds.setConnectionInitSqls(Collections.singletonList("set names utf8mb4"));
        try {
            ds.init();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return ds;
    }

    public static DruidDataSource getMysql137ProofDs() {
        Config config = ConfigFactory.load("mysql_137_proof.properties");
        String driver = config.getString("mysql.driver");
        String url = config.getString("mysql.url");
        String user = config.getString("mysql.user");
        String password = config.getString("mysql.password");

        DruidDataSource ds = new DruidDataSource();
        ds.setDriverClassName(driver);
        ds.setUrl(url);
        ds.setUsername(user);
        ds.setPassword(password);
        ds.setMaxActive(550);
        ds.setInitialSize(1);
        ds.setMinIdle(1);
        ds.setMaxActive(3);
        ds.setMaxWait(60000);
        ds.setTimeBetweenEvictionRunsMillis(60000);
        ds.setMinEvictableIdleTimeMillis(300000);
        ds.setValidationQuery("select 1");
        ds.setConnectionInitSqls(Collections.singletonList("set names utf8mb4"));
        try {
            ds.init();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return ds;
    }

    public static DruidDataSource getDorisAccountDs() {
        Config dorisConfig = ConfigFactory.load("doris_account.properties");
        String dorisDriver = dorisConfig.getString("doris.driver");
        String dorisUrl = dorisConfig.getString("doris.url");
        String dorisUsername = dorisConfig.getString("doris.username");
        String dorisPassword = dorisConfig.getString("doris.password");

        DruidDataSource ds = new DruidDataSource();
        ds.setDriverClassName(dorisDriver);
        ds.setUrl(dorisUrl);
        ds.setUsername(dorisUsername);
        ds.setPassword(dorisPassword);
        ds.setMaxActive(550);
        ds.setInitialSize(1);
        ds.setMinIdle(1);
        ds.setMaxActive(3);
        ds.setMaxWait(60000);
        ds.setTimeBetweenEvictionRunsMillis(60000);
        ds.setMinEvictableIdleTimeMillis(300000);
        ds.setValidationQuery("select 1");
        ds.setConnectionInitSqls(Collections.singletonList("set names utf8mb4"));
        try {
            ds.init();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return ds;
    }

    public static DruidDataSource getDorisBiDs() {
        Config dorisConfig = ConfigFactory.load("doris_bi.properties");
        String dorisDriver = dorisConfig.getString("doris.driver");
        String dorisUrl = dorisConfig.getString("doris.url");
        String dorisUsername = dorisConfig.getString("doris.username");
        String dorisPassword = dorisConfig.getString("doris.password");

        DruidDataSource ds = new DruidDataSource();
        ds.setDriverClassName(dorisDriver);
        ds.setUrl(dorisUrl);
        ds.setUsername(dorisUsername);
        ds.setPassword(dorisPassword);
        ds.setMaxActive(550);
        ds.setInitialSize(1);
        ds.setMinIdle(1);
        ds.setMaxActive(3);
        ds.setMaxWait(60000);
        ds.setTimeBetweenEvictionRunsMillis(60000);
        ds.setMinEvictableIdleTimeMillis(300000);
        ds.setValidationQuery("select 1");
        ds.setConnectionInitSqls(Collections.singletonList("set names utf8mb4"));
        try {
            ds.init();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return ds;
    }

    public static DruidDataSource getDorisBigFrogDs() {
        Config dorisConfig = ConfigFactory.load("doris_bigfrog.properties");
        String dorisDriver = dorisConfig.getString("doris.driver");
        String dorisUrl = dorisConfig.getString("doris.url");
        String dorisUsername = dorisConfig.getString("doris.username");
        String dorisPassword = dorisConfig.getString("doris.password");

        DruidDataSource ds = new DruidDataSource();
        ds.setDriverClassName(dorisDriver);
        ds.setUrl(dorisUrl);
        ds.setUsername(dorisUsername);
        ds.setPassword(dorisPassword);
        ds.setMaxActive(550);
        ds.setInitialSize(1);
        ds.setMinIdle(1);
        ds.setMaxActive(3);
        ds.setMaxWait(60000);
        ds.setTimeBetweenEvictionRunsMillis(60000);
        ds.setMinEvictableIdleTimeMillis(300000);
        ds.setValidationQuery("select 1");
        ds.setConnectionInitSqls(Collections.singletonList("set names utf8mb4"));
        try {
            ds.init();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return ds;
    }

    public static DruidDataSource getDorisProofDs() {
        Config dorisConfig = ConfigFactory.load("doris_proof.properties");
        String dorisDriver = dorisConfig.getString("doris.driver");
        String dorisUrl = dorisConfig.getString("doris.url");
        String dorisUsername = dorisConfig.getString("doris.username");
        String dorisPassword = dorisConfig.getString("doris.password");

        DruidDataSource ds = new DruidDataSource();
        ds.setDriverClassName(dorisDriver);
        ds.setUrl(dorisUrl);
        ds.setUsername(dorisUsername);
        ds.setPassword(dorisPassword);
        ds.setMaxActive(550);
        ds.setInitialSize(1);
        ds.setMinIdle(1);
        ds.setMaxActive(3);
        ds.setMaxWait(60000);
        ds.setTimeBetweenEvictionRunsMillis(60000);
        ds.setMinEvictableIdleTimeMillis(300000);
        ds.setValidationQuery("select 1");
        ds.setConnectionInitSqls(Collections.singletonList("set names utf8mb4"));
        try {
            ds.init();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return ds;
    }
}