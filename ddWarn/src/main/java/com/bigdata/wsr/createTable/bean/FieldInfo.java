package com.bigdata.wsr.createTable.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FieldInfo {
    private String fieldName;
    private String fieldType;
//    private String fieldDefault;
    private String fieldComment;
}
