package com.bigdata.wsr.dataCheck;


import com.bigdata.wsr.dataCheck.bean.DataCheckInfo;
import com.bigdata.wsr.dataCheck.utils.DingDingUtil;
import com.bigdata.wsr.dataCheck.utils.DataCheckUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * 钉钉 任务告警
 * <p>
 * https://blog.csdn.net/aaabbcq/article/details/123878496
 *
 * @author rui.wang
 * @date 2022/11/30
 */
@Slf4j
public class DingDingWarnTask {
    public static void main(String[] args) {
        List<DataCheckInfo> dataCheckInfoList = new ArrayList<>();
        DataCheckUtil.accountDbCheck(dataCheckInfoList);
        DataCheckUtil.biDbCheck(dataCheckInfoList);
        DataCheckUtil.bigFrogDbCheck(dataCheckInfoList);
        DataCheckUtil.proofDbCheck(dataCheckInfoList);
        for (DataCheckInfo dataCheckInfo : dataCheckInfoList) {
            if (dataCheckInfo.getDeltaNum() != 0) {
                String dingRequest = DingDingUtil.postWithJson(dataCheckInfo.toString());
                log.info("dingRequest -------> {}", dingRequest);
            }
        }
    }
}
