package com.bigdata.wsr.dataCheck.utils;

import com.alibaba.druid.pool.DruidDataSource;
import com.bigdata.wsr.createTable.config.DbConfig;
import com.bigdata.wsr.dataCheck.bean.DataCheckInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataCheckUtil {
    public static void accountDbCheck(List<DataCheckInfo> dataCheckInfoList) {
        List<String> tableList = new ArrayList<>();
        tableList.add("refund_info");
        tableList.add("tri_abate_info");

        // todo：source 库
        DruidDataSource ds = DbConfig.getMysql137AccountDs();
        Map<String, Integer> sourceTableCountMap = new HashMap<>();
        TableUtil.getSourceTableCount(tableList, sourceTableCountMap, ds);

        // todo：sink 库
        DruidDataSource dorisDs = DbConfig.getDorisAccountDs();
        Map<String, Integer> sinkTableCountMap = new HashMap<>();
        TableUtil.getSinkTableCount(tableList, sinkTableCountMap, dorisDs);

        for (String tableName : tableList) {
            dataCheckInfoList.add(
                    new DataCheckInfo(
                            "account",
                            tableName,
                            sourceTableCountMap.get(tableName),
                            sinkTableCountMap.get(tableName),
                            sourceTableCountMap.get(tableName) - sinkTableCountMap.get(tableName)
                    )
            );
        }
    }

    public static void biDbCheck(List<DataCheckInfo> dataCheckInfoList) {
        List<String> tableList = new ArrayList<>();
        tableList.add("aepayslip_rate2");
        tableList.add("bi_01_risk_pro");
        tableList.add("risk_customer_giftamt_detail");
        tableList.add("riskexpert_bill_month");
        tableList.add("saasfee_input");

        // todo：source 库
        DruidDataSource ds = DbConfig.getMysql137BiDs();
        Map<String, Integer> sourceTableCountMap = new HashMap<>();
        TableUtil.getSourceTableCount(tableList, sourceTableCountMap, ds);

        // todo：sink 库
        DruidDataSource dorisDs = DbConfig.getDorisBiDs();
        Map<String, Integer> sinkTableCountMap = new HashMap<>();
        TableUtil.getSinkTableCount(tableList, sinkTableCountMap, dorisDs);

        for (String tableName : tableList) {
            dataCheckInfoList.add(
                    new DataCheckInfo(
                            "bi",
                            tableName,
                            sourceTableCountMap.get(tableName),
                            sinkTableCountMap.get(tableName),
                            sourceTableCountMap.get(tableName) - sinkTableCountMap.get(tableName)
                    )
            );
        }
    }

    public static void bigFrogDbCheck(List<DataCheckInfo> dataCheckInfoList) {
        List<String> tableList = new ArrayList<>();
        tableList.add("clms_fee_package");
        tableList.add("clms_fee_pay_apply_bill");
        tableList.add("clms_fee_pay_apply_bill_fee");
        tableList.add("clms_product");
        tableList.add("ofs_make_loan");
        tableList.add("ofs_receipt_busi_detail");

        // todo：source 库
        DruidDataSource ds = DbConfig.getMysql137BigFrogDs();
        Map<String, Integer> sourceTableCountMap = new HashMap<>();
        TableUtil.getSourceTableCount(tableList, sourceTableCountMap, ds);

        // todo：sink 库
        DruidDataSource dorisDs = DbConfig.getDorisBigFrogDs();
        Map<String, Integer> sinkTableCountMap = new HashMap<>();
        TableUtil.getSinkTableCount(tableList, sinkTableCountMap, dorisDs);

        for (String tableName : tableList) {
            dataCheckInfoList.add(
                    new DataCheckInfo(
                            "bigfrog",
                            tableName,
                            sourceTableCountMap.get(tableName),
                            sinkTableCountMap.get(tableName),
                            sourceTableCountMap.get(tableName) - sinkTableCountMap.get(tableName)
                    )
            );
        }
    }

    public static void proofDbCheck(List<DataCheckInfo> dataCheckInfoList) {
        List<String> tableList = new ArrayList<>();
        tableList.add("contract_solution");

        // todo：source 库
        DruidDataSource ds = DbConfig.getMysql137ProofDs();
        Map<String, Integer> sourceTableCountMap = new HashMap<>();
        TableUtil.getSourceTableCount(tableList, sourceTableCountMap, ds);

        // todo：sink 库
        DruidDataSource dorisDs = DbConfig.getDorisProofDs();
        Map<String, Integer> sinkTableCountMap = new HashMap<>();
        TableUtil.getSinkTableCount(tableList, sinkTableCountMap, dorisDs);

        for (String tableName : tableList) {
            dataCheckInfoList.add(
                    new DataCheckInfo(
                            "proof",
                            tableName,
                            sourceTableCountMap.get(tableName),
                            sinkTableCountMap.get(tableName),
                            sourceTableCountMap.get(tableName) - sinkTableCountMap.get(tableName)
                    )
            );
        }
    }

}
