package com.bigdata.wsr.dataCheck;

import com.bigdata.wsr.dataCheck.bean.DataCheckInfo;
import com.bigdata.wsr.dataCheck.utils.DataCheckUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据核对
 *
 * @author rui.wang
 * @date 2022/11/25
 */
public class DataCheckTask {
    public static void main(String[] args) {
        List<DataCheckInfo> dataCheckInfoList = new ArrayList<>();
        DataCheckUtil.accountDbCheck(dataCheckInfoList);
        DataCheckUtil.biDbCheck(dataCheckInfoList);
        DataCheckUtil.bigFrogDbCheck(dataCheckInfoList);
        DataCheckUtil.proofDbCheck(dataCheckInfoList);
        for (DataCheckInfo dataCheckInfo : dataCheckInfoList) {
            System.out.println(dataCheckInfo);
        }
    }
}
